﻿using System;
using System.Windows;
using BloodApp.Model;
using caalhp.IcePluginAdapters;
using caalhp.IcePluginAdapters.WPF;
using GalaSoft.MvvmLight;

namespace BloodApp.ViewModel
{
    /// <summary>
    ///     This class contains properties that the main View can data bind to.
    ///     <para>
    ///         See http://www.galasoft.ch/mvvm
    ///     </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private BloodyModel _model;
        private AppAdapter _adapter;

        private double _sysValue;
        private double _diaValue;

        /// <summary>
        ///     Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            SysValue = 120.4;
            DiaValue = 80;
            //Register with CAALHP:
            ConnectToCaalhp();
        }

        /// <summary>
        /// A systolic value that can be shown via databinding
        /// </summary>
        public double SysValue
        {
            get { return _sysValue; }
            set
            {
                _sysValue = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// A diastolic value that can be shown via databinding
        /// </summary>
        public double DiaValue
        {
            get { return _diaValue; }
            set
            {
                _diaValue = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Handles connecting to the host (CAALHP)
        /// </summary>
        private void ConnectToCaalhp()
        {
            const string endpoint = "localhost";
            try
            {
                //save a reference to a bloodymodel. It is injected with "this" to enable it to call us.
                _model = new BloodyModel(this);
                //create an appadapter that will handle inter process communication
                _adapter = new AppAdapter(endpoint, _model);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// Pop window to the front
        /// </summary>
        public void Show()
        {
            Helper.BringToFront();
        }
    }
}