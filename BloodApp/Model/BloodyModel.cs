﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using BloodApp.ViewModel;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;

namespace BloodApp.Model
{
    public class BloodyModel : IAppCAALHPContract
    {
        private readonly MainViewModel _main;
        private IAppHostCAALHPContract _host;
        private int _processId;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="main">A reference to the MainViewModel so that we can call it later</param>
        public BloodyModel(MainViewModel main)
        {
            _main = main;
        }

        /// <summary>
        /// This is called when an event we subscibed to is fired.
        /// </summary>
        /// <param name="notification">a serialized version of an event. 
        /// We need to deserialize it to use it in our app</param>
        public void Notify(KeyValuePair<string, string> notification)
        {
            var theType = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            var receivedObject = JsonSerializer.DeserializeEvent(notification.Value, theType);
            var measurementEvent = receivedObject as BloodPressureMeasurementEvent;
            if (measurementEvent == null) return;
            _main.SysValue = measurementEvent.Systolic;
            _main.DiaValue = measurementEvent.Diastolic;
        }

        /// <summary>
        /// This just returns the name of our app
        /// </summary>
        /// <returns>the name of our app</returns>
        public string GetName()
        {
            return "BloodApp";
        }

        /// <summary>
        /// This is called every second, and if we don't respond with a TRUE.
        /// the CAALHP might think we are dead, and eventually will kill our process.
        /// </summary>
        /// <returns></returns>
        public bool IsAlive()
        {
            return true;
        }

        /// <summary>
        /// This is our chance to close down our app graciously.
        /// </summary>
        public void ShutDown()
        {
            Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        /// <summary>
        /// This is called when our app has been registered by the CAALHP
        /// We handle initialization by storing a reference to the host and our processId.
        /// Furthermore we subscribe to events that are relevant to our app.
        /// </summary>
        /// <param name="hostObj">a reference to the host(CAALHP)</param>
        /// <param name="processId">our registered process ID</param>
        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            var fullyQualifiedNameSpace = EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json,
                typeof (BloodPressureMeasurementEvent));
            _host.Host.SubscribeToEvents(fullyQualifiedNameSpace, _processId);
        }

        /// <summary>
        /// This is called when the app is clicked from the homescreen,
        /// so we have to do what i needed to make our window go to the front
        /// </summary>
        public void Show()
        {
            Application.Current.Dispatcher.Invoke(() => _main.Show(), DispatcherPriority.Normal);
        }
    }
}